const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");

const musicFolder = "./music";
const nbOfSongs = parseInt(await fetch("./index.txt").then((r) => r.text()));

async function main() {
  resizeCanvas();

  // Audio Configuration
  const audio = new Audio(getRandomMusic());
  audio.crossOrigin = "anonymous";
  const audioContext = new AudioContext();
  const source = audioContext.createMediaElementSource(audio);

  if (audioContext.state === "suspended") {
    // Background
    ctx.fillStyle = "#0f1020";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Text
    ctx.fillStyle = "#ffffff";
    ctx.textAlign = "center";
    ctx.fillText("Click here", canvas.width / 2, canvas.height / 2);

    canvas.addEventListener("click", main, { once: true });
    return;
  }

  const analyser = audioContext.createAnalyser();
  analyser.fftSize = 32;
  const bufferLength = analyser.frequencyBinCount;
  const dataArray = new Uint8Array(bufferLength);

  source.connect(analyser);
  analyser.connect(audioContext.destination);

  // Image Loading
  const image = new Image();
  image.src = "https://kelbazz.github.io/assets/img/loko_graph.png";

  const minSize = 250;
  const maxSize = 400;
  const bassThreshold = 50;

  const dotSpacing = 50;
  const minDotSize = 0;
  const maxDotSize = 5;

  let logoSize;
  let dots = [];

  image.onload = () => {
    calculateLogoSize();
    createDots();
    drawVisualizer();
  };

  function resizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
  }

  function getRandomMusic() {
    return `${musicFolder}/music-${Math.round(Math.random() * nbOfSongs - 1)
      .toString()
      .padStart(4, "0")}.mp3`;
  }

  function calculateLogoSize() {
    const maxLogoSize = Math.min(canvas.width, canvas.height) - 2;
    logoSize = Math.min(maxLogoSize, maxSize);
  }

  function createDots() {
    const numCols = Math.floor(canvas.width / dotSpacing);
    const numRows = Math.floor(canvas.height / dotSpacing);
    const leftMargin = (canvas.width - numCols * dotSpacing) / 2;
    const topMargin = (canvas.height - numRows * dotSpacing) / 2;

    for (let i = 0; i < numCols; i++) {
      for (let j = 0; j < numRows; j++) {
        const dot = {
          x: leftMargin + i * dotSpacing + dotSpacing / 2,
          y: topMargin + j * dotSpacing + dotSpacing / 2,
          size: 5,
          reactionType: getRandomReactionType(),
          reactionAmount: 0,
        };
        dots.push(dot);
      }
    }
  }

  function getRandomReactionType() {
    const reactionTypes = ["mids", "highs", "lows"];
    return reactionTypes[Math.floor(Math.random() * reactionTypes.length)];
  }

  function drawVisualizer() {
    ctx.fillStyle = "#0f1020";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    analyser.getByteFrequencyData(dataArray);

    let sum = 0;
    for (let i = 0; i < bufferLength; i++) {
      sum += dataArray[i];
    }

    const average = sum / bufferLength;

    const scaleFactor = Math.min(Math.max(average, bassThreshold), 256) / 256;

    const scaledImageWidth = minSize + scaleFactor * (logoSize - minSize);
    const scaledImageHeight = scaledImageWidth * (image.height / image.width);

    const xPos = (canvas.width - scaledImageWidth) / 2;
    const yPos = (canvas.height - scaledImageHeight) / 2;

    // Update and draw the dots
    dots.forEach((dot) => {
      let reactionValue;

      switch (dot.reactionType) {
        case "mids":
          reactionValue = dataArray[Math.floor(bufferLength / 3)];
          break;
        case "highs":
          reactionValue = dataArray[Math.floor((bufferLength / 3) * 2)];
          break;
        case "lows":
          reactionValue = dataArray[Math.floor(bufferLength / 6)];
          break;
        default:
          reactionValue = 0;
          break;
      }

      dot.reactionAmount = reactionValue / 255;

      dot.size = minDotSize + dot.reactionAmount * (maxDotSize - minDotSize);

      ctx.fillStyle = `rgba(255, 255, 255, ${dot.reactionAmount})`;
      ctx.beginPath();
      ctx.arc(dot.x, dot.y, dot.size, 0, 2 * Math.PI);
      ctx.fill();
    });

    // Draw the logo
    ctx.drawImage(image, xPos, yPos, scaledImageWidth, scaledImageHeight);

    requestAnimationFrame(drawVisualizer);
  }

  // Call this on resize
  window.addEventListener("resize", () => {
    resizeCanvas();
    calculateLogoSize();
    dots = [];
    createDots();
  });

  canvas.addEventListener("click", () =>
    audio.paused ? audio.play() : audio.pause()
  );

  function nextSong() {
    audio.src = getRandomMusic();
    audio.play();
  }

  audio.addEventListener("ended", nextSong);
  canvas.addEventListener("dblclick", nextSong);

  audio.play();
}

main();
