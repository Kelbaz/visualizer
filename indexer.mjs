import * as fs from "fs";

fs.readdirSync("./music").forEach((file, i) => {
  const idx = i.toString().padStart(4, "0");
  fs.renameSync(`./music/${file}`, `./music/music-${idx}.mp3`);
});

fs.writeFileSync("./index.txt", fs.readdirSync("./music").length.toString());
