const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");

const audio = document.querySelector("#audio") /* || new Audio("music.mp3") */;
// audio.crossOrigin = "anonymous";
const audioContext = new AudioContext();
const source = audioContext.createMediaElementSource(audio);

const analyser = audioContext.createAnalyser();
analyser.fftSize = 32;
const bufferLength = analyser.frequencyBinCount;
const dataArray = new Uint8Array(bufferLength);

source.connect(analyser);
analyser.connect(audioContext.destination);

const image = new Image();
image.src = "https://kelbazz.github.io/assets/img/loko_graph.png";

let imageWidth = 300;
let imageHeight = 200;

const minSize = 50;
const maxSize = 200;
const bassThreshold = 100;

image.onload = () => {
  const ratio = image.width / image.height;
  imageHeight = imageWidth / ratio;
  drawVisualizer();
};

function drawVisualizer() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const barWidth = canvas.width / bufferLength;
  const barHeightMultiplier = canvas.height / 256;

  analyser.getByteFrequencyData(dataArray);

  let sum = 0;
  for (let i = 0; i < bufferLength; i++) {
    sum += dataArray[i];
  }

  const average = sum / bufferLength;

  const scaleFactor = Math.min(Math.max(average, bassThreshold), 256) / 256;
  const scaledImageWidth = minSize + scaleFactor * (maxSize - minSize);
  const scaledImageHeight = scaledImageWidth * (imageHeight / imageWidth);

  const xPos = (canvas.width - scaledImageWidth) / 2;
  const yPos = (canvas.height - scaledImageHeight) / 2;

  ctx.drawImage(image, xPos, yPos, scaledImageWidth, scaledImageHeight);

  requestAnimationFrame(drawVisualizer);
}

// audio.play();